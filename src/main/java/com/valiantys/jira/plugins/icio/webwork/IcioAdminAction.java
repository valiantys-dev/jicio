package com.valiantys.jira.plugins.icio.webwork;

import com.atlassian.jira.web.action.JiraWebActionSupport;

/**
 * Web actions to edit and update the LNIO PLUGIN configuration.<br>
 * You can add specific fields mapping for a context.<br>
 * 
 * @author Maxime
 * @since v 1.4.0
 */
public class IcioAdminAction extends JiraWebActionSupport {

    /**
     * The serial ID.
     */
    private static final long serialVersionUID = 351916174672386910L;

    /**
     * Display the valiantys disclaimer.
     * 
     * @return
     */
    public final String doDisplayDisclaimer() {
	return SUCCESS;
    }
}
