package com.valiantys.jira.plugins.icio.webwork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayout;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutItem;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.link.LinkCollection;
import com.atlassian.jira.issue.link.RemoteIssueLinkManager;
import com.atlassian.jira.issue.views.util.IssueViewUtil;
import com.atlassian.jira.plugin.assignee.AssigneeResolver;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.action.issue.CloneIssueDetails;
import com.atlassian.jira.web.action.issue.IssueCreationHelperBean;
import com.atlassian.jira.web.component.IssueTableLayoutBean;
import com.atlassian.jira.web.component.SimpleColumnLayoutItem;
import com.valiantys.jira.plugins.icio.business.SubTaskCloneManager;

/**
 * This class is used to clone an issue, with its selected subtasks only.
 * 
 * @author Agar Mathieu <mathieu.agar@valiantys.com>
 * 
 */
public class ImprovedCloneIssueAction extends CloneIssueDetails {

	private final static long serialVersionUID = 3639979411607855713L;
	private final static Logger LOG = Logger.getLogger(ImprovedCloneIssueAction.class);

	private final ColumnLayoutManager columnLayoutManager;
    private final IssueViewUtil issueViewUtil;
    private final JiraAuthenticationContext authContext;
    private final I18nHelper.BeanFactory i18nHelperFactory;
    
    /**
     * String used to identify a sub task checkbox
     */
    public final static String CHECK_SUBTASK = "SubTaskCheck";
    /**
     * String used to identify an inward link checkbox
     */
    public final static String CHECK_LINK_IN = "LinkCheckIn";
    /**
     * String used to identify a outward link checkbox
     */
    public final static String CHECK_LINK_OUT = "LinkCheckOut";
    public final static String COLUMNS_SUBTASK = "cloneissuewithsubtask.columns.subtasks";

    private boolean cloneSubTasksLinks;
    private ConstantsManager constantsManager;
    private final SubTaskCloneManager subTaskCloneManager;
    private final AssigneeResolver assigneeResolver;
    
private final IssueLinkManager issueLinkManager;
    
    /**
     * 
     * @param applicationProperties
     * @param permissionManager
     * @param issueLinkManager
     * @param issueLinkTypeManager
     * @param subTaskManager
     * @param fieldManager
     * @param issueCreationHelperBean
     * @param issueFactory
     * @param columnLayoutManager
     * @param issueManager
     * @param issueViewUtil
     */
    public ImprovedCloneIssueAction(final ApplicationProperties applicationProperties, final PermissionManager permissionManager,
            final IssueLinkManager issueLinkManager, final RemoteIssueLinkManager remoteIssueLinkManager,
            final IssueLinkTypeManager issueLinkTypeManager, final SubTaskManager subTaskManager, final AttachmentManager attachmentManager,
            final FieldManager fieldManager, final IssueCreationHelperBean issueCreationHelperBean, final IssueFactory issueFactory,
            final ColumnLayoutManager columnLayoutManager, final IssueViewUtil issueViewUtil, final IssueService issueService,
            final JiraAuthenticationContext aAuthContext, final I18nHelper.BeanFactory aI18nHelperFactory, final ConstantsManager constantsManager,
            SubTaskCloneManager subTaskCloneManager, AssigneeResolver assigneeResolver) {

    	super(applicationProperties, permissionManager, issueLinkManager,
                remoteIssueLinkManager, issueLinkTypeManager, subTaskManager, attachmentManager, fieldManager, issueCreationHelperBean, issueFactory,
                issueService);
        this.columnLayoutManager = columnLayoutManager;
        this.issueViewUtil = issueViewUtil;
        this.authContext = aAuthContext;
        this.i18nHelperFactory = aI18nHelperFactory;
        this.constantsManager = constantsManager;
        this.subTaskCloneManager = subTaskCloneManager;
        this.assigneeResolver = assigneeResolver;
        this.issueLinkManager = issueLinkManager;
        
    }

    @Override
    public String doDefault() throws Exception {
        String result = super.doDefault();
        request.setAttribute("itlb", getIssueTableLayoutBeanSubTask());
        request.setAttribute("i18nIcio", i18nHelperFactory.getInstance(authContext.getLoggedInUser()));
        return result;
    }

    @Override
    protected String doExecute() {
    	super.setCloneSubTasks(false);
    	setDefaultAssignee();
    	String returnValue = super.doExecute();
    	subTaskCloneManager.createSubTask(getOriginalIssue(), getIssueObject(), getSelectedSubtasks(), authContext.getLoggedInUser());
    	subTaskCloneManager.createLinks();
    	subTaskCloneManager.createLinksOnOrigionalIssue(getInwardLinks(), getOutwardLinks(),getIssueObject());
    	return returnValue;
    }
    
    // Get the default value for the automatic assignee
    private void setDefaultAssignee(){
        Map<String, Object> fieldValuesHolder = new HashMap<String, Object>();
        List<Long> componentsId = null;
        if (super.getIssueObject().getComponentObjects() != null) {
            for (ProjectComponent c : super.getIssueObject().getComponentObjects()) {
                if (componentsId == null) {
                    componentsId = new ArrayList<Long>();
                }
                componentsId.add(c.getId());
            }
        }
        fieldValuesHolder.put("components", componentsId);
        super.getIssueObject().setAssignee(assigneeResolver.getDefaultAssigneeObject(super.getIssueObject(), fieldValuesHolder));
    }

    /**
     * Using parameters to set selectedSubtasks value.
     */
    private List<Long> getSelectedSubtasks() {
        Map params = request.getParameterMap();
        List<Long> selectedSubtasks = new ArrayList<Long>();

        for (Object o : params.entrySet()) {
            Map.Entry entry = (Map.Entry) o;

            String key = (String) entry.getKey();
            if (entry.getValue() != null && key.startsWith(CHECK_SUBTASK)) {
                selectedSubtasks.add(Long.valueOf(key.substring(CHECK_SUBTASK.length())));
            }
        }
        return selectedSubtasks;
    }
    
    private  List<IssueLink> getInwardLinks()
    { 
    	return getLinks(true, issueLinkManager.getInwardLinks(getOriginalIssue().getId()), "LinkCheckIn_");
    }
    
    private  List<IssueLink> getOutwardLinks()
    {
    	return getLinks(false, issueLinkManager.getOutwardLinks(getOriginalIssue().getId()), "LinkCheckOut_");
    }
    
    private  List<IssueLink> getLinks(boolean isInward, List<IssueLink> links, String prefix){
    	Map params = request.getParameterMap();
    	
    	List<IssueLink> selectedLinks = new ArrayList<IssueLink>();
      
        if(links!=null && links.size()>0){
        for (IssueLink l : links) {
        	  if(isInward){
              	if(params.get((new StringBuilder()).append(prefix).append(l.getLinkTypeId()).append("_").append(l.getSourceId()).toString()) != null)
                  {
              		selectedLinks.add(l);
                  }
              }else{
              	if(params.get((new StringBuilder()).append(prefix).append(l.getLinkTypeId()).append("_").append(l.getDestinationId()).toString()) != null)
                  {
              		selectedLinks.add(l);
                  }
          	}
		}
        }
      return selectedLinks;
        
    }
    

    /**
     * 
     * @param clone
     */
    public void setCloneSubTasksLinks(final boolean clone) {
        this.cloneSubTasksLinks = clone;
    }

    /**
     * 
     * @return
     */
    public boolean isCloneSubTasksLinks() {
        return this.cloneSubTasksLinks;
    }

    /**
     * 
     * @return a List of subTaskObject
     */
    public List<Issue> getSubTasksList() {
        LOG.debug("getSubTasksList : Nombre de subtasks : " + getOriginalIssue().getSubTaskObjects().size());
        return ((List<Issue>) getOriginalIssue().getSubTaskObjects());
    }

    /**
     * 
     * @return
     */
    public LinkCollection getLinkCollection() {
        LOG.debug("Links : " + issueViewUtil.getLinkCollection(getOriginalIssue(), getLoggedInUser()).getLinkTypes().size());
        return (issueViewUtil.getLinkCollection(getOriginalIssue(), getLoggedInUser()));
    }
    
    /**
     * 
     * @return the column element to add at the beginning of the table.
     */
    private SimpleColumnLayoutItem getCheckboxColumn() {
        return new SimpleColumnLayoutItem() {
            @Override
            public String getHtml(final Map displayParams, final Issue issue) {
                return "<input type='checkbox' checked name='" + CHECK_SUBTASK + issue.getId() + "'>";
            }

            @Override
            public String getHeaderHtml() {
                return "<input type='checkbox' checked name='all" + ImprovedCloneIssueAction.CHECK_SUBTASK + "' onClick='setCheckboxes"
                        + ImprovedCloneIssueAction.CHECK_SUBTASK + "()'>";

            }
        };
    }

    /**
     * 
     * @param columnsToDisplay columns defined in properties file.
     * @return
     * @throws Exception
     */
    public List<ColumnLayoutItem> getColumns(final String columnsToDisplay) throws Exception {
        LOG.debug("columnsToDisplay :" + columnsToDisplay);

        ColumnLayout columnLayout = columnLayoutManager.getColumnLayout(getLoggedInUser());
        String[] filters = getFilters(columnsToDisplay);
        // needed to create visible layout items
        List<String> types = new ArrayList<String>();
        List<Long> projects = new ArrayList<Long>();
        projects.add(getOriginalIssue().getProjectObject().getId());
        // types will contain sub tasks types.
        types.add(ConstantsManager.ALL_SUB_TASK_ISSUE_TYPES);
        types = constantsManager.expandIssueTypeIds(types);

        ArrayList<ColumnLayoutItem> columns = new ArrayList<ColumnLayoutItem>();
        LOG.debug("getColumns : size of subtasks types List :" + types.size());

        List<ColumnLayoutItem> visibleColumns = columnLayout.getAllVisibleColumnLayoutItems(getLoggedInUser());

        // Filtering the visible columns, displaying only those specified in the
        // properties file
        for (Iterator<ColumnLayoutItem> it = visibleColumns.iterator(); it.hasNext();) {
            ColumnLayoutItem cli = (ColumnLayoutItem) it.next();
            String name = cli.getColumnHeadingKey();
            if (name.indexOf("issue.column.heading.") > -1) {
                name = (StringUtils.split(cli.getColumnHeadingKey(), '.')[3]);
            }

            for (int i = 0; i < filters.length; i++) {
                LOG.debug("filters[i] :" + filters[i] + " - name :" + name);
                if (filters[i].equals(name)) {
                    columns.add(cli);
                }
            }
        }

        // checkbox column,
        columns.add(0, getCheckboxColumn());

        return columns;

    }

    /**
     * Exploding the comma-separated values from the string in properties file.
     * 
     * @param columnsToDisplay
     * @return
     */
    public String[] getFilters(final String columnsToDisplay) {
        String filter = i18nHelperFactory.getInstance(authContext.getLoggedInUser()).getText(columnsToDisplay);
        String[] filters = StringUtils.split(filter, ',');
        return (filters);
    }

    /**
     * 
     * @return
     * @throws Exception
     */
    public IssueTableLayoutBean getIssueTableLayoutBeanSubTask() throws Exception {
        LOG.debug("COLUMNS_SUBTASK : " + COLUMNS_SUBTASK);
        return (this.getIssueTableLayoutBean(COLUMNS_SUBTASK));
    }

    /**
     * 
     * @param columnsToDisplay
     * @return
     * @throws Exception
     */
    public IssueTableLayoutBean getIssueTableLayoutBean(final String columnsToDisplay) throws Exception {

        log.debug("getIssueTableLayoutBean");
        IssueTableLayoutBean layoutBean = new IssueTableLayoutBean(getColumns(columnsToDisplay));
        layoutBean.setSortingEnabled(false);
        return layoutBean;
    }

}
