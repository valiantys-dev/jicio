package com.valiantys.jira.plugins.icio.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.link.IssueLink;

/**
 * This interface allows to create the subtasks and the link between those under the cloned issue
 * @author CELAD
 *
 */
public interface SubTaskCloneManager {
	
	/**
	 * Get the selected sub-tasks under original issue and create the same under clonedIssue
	 * @param originalIssue
	 * @param clonedIssue
	 * @param selectedSubTasks
	 * @return a map with all the sub-tasks under originalIssue and the corresponding subTask under clonedIssue
	 * if there is no corresponding subTask under clonedIssue, the value is null
	 */
	Map<Issue, Issue> createSubTask(Issue originalIssue, Issue clonedIssue, List<Long> selectedSubTasks, User user);
	
	/**
	 * Create the consolidation links between the cloned subtasks. 
	 */
	void createLinks();
	 
	void createLinksOnOrigionalIssue(List<IssueLink> inwardLinks, List<IssueLink> outwardLinks, Issue cloneIssueId);
}
