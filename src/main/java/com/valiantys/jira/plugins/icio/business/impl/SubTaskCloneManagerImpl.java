package com.valiantys.jira.plugins.icio.business.impl;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.IssueService.CreateValidationResult;
import com.atlassian.jira.bc.issue.link.IssueLinkService;
import com.atlassian.jira.bc.issue.link.IssueLinkService.AddIssueLinkValidationResult;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.link.Direction;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.util.AttachmentUtils;
import com.atlassian.jira.web.util.AttachmentException;
import com.valiantys.jira.plugins.icio.business.SubTaskCloneManager;

public class SubTaskCloneManagerImpl implements SubTaskCloneManager {

	private final static Logger LOG = Logger
			.getLogger(SubTaskCloneManager.class);

	private final IssueManager issueManager;
	private final IssueLinkManager issueLinkManager;
	private final IssueLinkService issueLinkService;

	private final IssueLinkTypeManager issueLinkTypeManager;
	private final IssueService issueService;
	private final AttachmentManager attachmentManager;
	private final SubTaskManager subTaskManager;

	private User user;
	/**
	 * Map<old issue, cloned issue>
	 */
	private Map<Issue, Issue> clonedSubTasks;

	public SubTaskCloneManagerImpl(IssueManager issueManager,
			IssueLinkManager issueLinkManager,
			IssueLinkTypeManager issueLinkTypeManager,
			IssueService issueService, AttachmentManager attachmentManager,
			SubTaskManager subTaskManager,
			final IssueLinkService issueLinkService) {
		this.issueManager = issueManager;
		this.issueLinkManager = issueLinkManager;
		this.issueLinkTypeManager = issueLinkTypeManager;
		this.issueService = issueService;
		this.attachmentManager = attachmentManager;
		this.subTaskManager = subTaskManager;
		this.issueLinkService = issueLinkService;
	}

	public Map<Issue, Issue> createSubTask(Issue originalIssue,
			Issue clonedIssue, List<Long> selectedSubTasks, User user) {
		this.user = user;
		clonedSubTasks = new HashMap<Issue, Issue>();
		Collection<Issue> subTasks = originalIssue.getSubTaskObjects();
		for (Issue subTask : subTasks) {
			Issue newSubTask = null;
			if (selectedSubTasks.contains(subTask.getId())) {
				try {
					Issue clonedSubTaskIssue = createIssue(clonedIssue, subTask);
					if (clonedSubTaskIssue != null) {
						newSubTask = issueManager.createIssueObject(user,
								clonedSubTaskIssue);
						try {
							subTaskManager.createSubTaskIssueLink(clonedIssue,
									newSubTask, user);
							createAttachment(subTask, newSubTask);
						} catch (CreateException e) {
							e.printStackTrace();
						}
					}
				} catch (CreateException e) {
					e.printStackTrace();
				}
			}
			clonedSubTasks.put(subTask, newSubTask);
		}
		return clonedSubTasks;
	}

	/**
	 * Create a new issue with the same fields as the parameter subTask issue
	 * 
	 * @param parentIssue
	 * @param subTask
	 * @return the new issue
	 */
	private Issue createIssue(Issue parentIssue, Issue subTask) {
		IssueInputParameters parameters = populateParameters(subTask,
				parentIssue);
		CreateValidationResult createValidationResult = issueService
				.validateSubTaskCreate(user, parentIssue.getId(), parameters);
		if (createValidationResult.getErrorCollection().hasAnyErrors()) {
			LOG.error(createValidationResult.getErrorCollection());
		}
		return createValidationResult.getIssue();
	}

	/**
	 * Create all the attachments in the clonedIssue with the attachments
	 * already present in the original issue
	 * 
	 * @param originalIssue
	 * @param clonedIssue
	 */
	private void createAttachment(Issue originalIssue, Issue clonedIssue) {
		List<Attachment> attachments = this.attachmentManager
				.getAttachments(originalIssue);
		for (Attachment attachment : attachments) {
			File attachmentFile = AttachmentUtils.getAttachmentFile(attachment);
			if ((attachmentFile.exists()) && (attachmentFile.canRead())) {
				try {
					this.attachmentManager.createAttachmentCopySourceFile(
							attachmentFile, attachment.getFilename(),
							attachment.getMimetype(), user.getName(),
							clonedIssue, new HashMap<String, Object>(),
							new Timestamp(System.currentTimeMillis()));
				} catch (AttachmentException e) {
					LOG.warn(
							new StringBuilder()
									.append("Could not clone attachment with id '")
									.append(attachment.getId())
									.append("' and file path '")
									.append(attachmentFile.getAbsolutePath())
									.append("' for issue with id '")
									.append(clonedIssue.getId())
									.append("' and key '")
									.append(clonedIssue.getKey()).append("'.")
									.toString(), e);
				}
			} else {
				LOG.warn(new StringBuilder()
						.append("Could not clone attachment with id '")
						.append(attachment.getId())
						.append("' and file path '")
						.append(attachmentFile.getAbsolutePath())
						.append("' for issue with id '")
						.append(clonedIssue.getId())
						.append("' and key '")
						.append(clonedIssue.getKey())
						.append("', ")
						.append("because the file path ")
						.append((attachmentFile.exists()) ? "is not readable."
								: "does not exist.").toString());
			}
		}
	}

	private IssueInputParameters populateParameters(Issue subTask,
			Issue parentIssue) {
		IssueInputParameters parameters = issueService
				.newIssueInputParameters();
		parameters.setIssueTypeId(subTask.getIssueTypeObject().getId());
		parameters.setProjectId(subTask.getProjectObject().getId());
		parameters.setSummary(subTask.getSummary());
		parameters.setAssigneeId(parentIssue.getAssigneeId());
		parameters.setDescription(subTask.getDescription());
		parameters.setPriorityId(subTask.getPriorityObject().getId());
		parameters.setReporterId(subTask.getReporterId());
		parameters.setStatusId(subTask.getStatusObject().getId());
		parameters.setSecurityLevelId(subTask.getSecurityLevelId());
		parameters.setEnvironment(subTask.getEnvironment());
		Long[] fixVersions = new Long[subTask.getFixVersions().size()];
		Iterator<Version> it = subTask.getFixVersions().iterator();
		int i = 0;
		while (it.hasNext()) {
			fixVersions[i] = it.next().getId();
			i++;
		}
		parameters.setFixVersionIds(fixVersions);
		Long[] affectedVersions = new Long[subTask.getAffectedVersions().size()];
		Iterator<Version> ite = subTask.getAffectedVersions().iterator();
		int j = 0;
		while (ite.hasNext()) {
			affectedVersions[j] = ite.next().getId();
			j++;
		}
		parameters.setAffectedVersionIds(affectedVersions);
		Long[] componentIds = new Long[subTask.getComponentObjects().size()];
		Iterator<Version> iterator = subTask.getAffectedVersions().iterator();
		int k = 0;
		while (iterator.hasNext()) {
			componentIds[k] = iterator.next().getId();
			k++;
		}
		parameters.setComponentIds(componentIds);
		return parameters;
	}

	public void createLinks() {
		if (clonedSubTasks == null || clonedSubTasks.isEmpty()) {
			return;
		}
		for (Map.Entry<Issue, Issue> clonedSubTask : clonedSubTasks.entrySet()) {
			Issue oldIssue = clonedSubTask.getKey();

			List<IssueLink> outwardIssueLinks = issueLinkManager
					.getOutwardLinks(clonedSubTask.getKey().getId());
			List<IssueLink> inwardIssueLinks = issueLinkManager
					.getInwardLinks(clonedSubTask.getKey().getId());
			Long parentId = clonedSubTask.getKey().getParentId();

			Long cloneIssueId = clonedSubTask.getValue() != null ? clonedSubTask
					.getValue().getId() : clonedSubTask.getKey().getId();
			createLinksOnOrigionalIssue(inwardIssueLinks, outwardIssueLinks,
					issueManager.getIssueObject(cloneIssueId));
		}
	}

	public void createLinksOnOrigionalIssue(List<IssueLink> inwardIssueLinks,
			List<IssueLink> outwardIssueLinks, Issue cloneIssueId) {
		List<String> issueKeys = null;
		for (IssueLink issueLink : outwardIssueLinks) {
			Issue destinationIssue = issueLink.getDestinationObject();
			issueKeys = new ArrayList<String>();
			issueKeys.add(destinationIssue.getKey());
			AddIssueLinkValidationResult result = issueLinkService
					.validateAddIssueLinks(user, cloneIssueId,
							issueLink.getLinkTypeId(), Direction.OUT,
							issueKeys, false);
			issueLinkService.addIssueLinks(user, result);
		}

		for (IssueLink issueLink : inwardIssueLinks) {
			Issue sourceIssue = issueLink.getSourceObject();
			issueKeys = new ArrayList<String>();
			issueKeys.add(sourceIssue.getKey());
			AddIssueLinkValidationResult result = issueLinkService
					.validateAddIssueLinks(user, cloneIssueId,
							issueLink.getLinkTypeId(), Direction.IN, issueKeys,
							false);
			issueLinkService.addIssueLinks(user, result);
		}
	}

}
