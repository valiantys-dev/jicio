<%@ page import="com.atlassian.jira.ComponentManager" %>
<%@ page import="com.atlassian.jira.plugin.keyboardshortcut.KeyboardShortcutManager" %>

<%@ page import="java.util.List, java.util.Iterator"%>
<%@ page import="com.atlassian.jira.issue.Issue,
com.atlassian.jira.web.component.IssueTableLayoutBean,
com.atlassian.jira.web.component.IssueTableWebComponent,
com.atlassian.jira.issue.link.LinkCollection,
com.atlassian.jira.issue.link.IssueLinkType,
com.atlassian.jira.util.I18nHelper"%>

<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib prefix="aui" uri="webwork" %>
<%@ taglib uri="sitemesh-page" prefix="page"%>

<%@ taglib uri="webwork" prefix="iterator"%>

<ww:property value="/issueTableLayoutBeanSubTask" id="itlb" />
<ww:property value="/subTasksList" id="subTasksList" />
<ww:property value="/linkCollection" id="linkCollection" />

<%
IssueTableLayoutBean itlb = (IssueTableLayoutBean) request.getAttribute("itlb");
List subtasks = (List) request.getAttribute("subTasksList");
LinkCollection links = (LinkCollection) request.getAttribute("linkCollection");

final String CHECK_SUBTASK="SubTaskCheck";
final String CHECK_LINK_IN="LinkCheckIn";
final String CHECK_LINK_OUT="LinkCheckOut";

I18nHelper i18nIcio = (I18nHelper) request.getAttribute("i18nIcio");
String linkLabel = i18nIcio.getText("cloneissuewithsubtask.clonesubtaskslinks.label");
String linkDesc = i18nIcio.getText("cloneissuewithsubtask.clonesubtaskslinks.description");
%>

<html>
<head>

<ww:if test="/issueValid == true && /originalIssue != null && /hasIssuePermission('create', /issue) == true"><meta content="issueaction" name="decorator" /></ww:if>
    <ww:else><meta content="message" name="decorator" /></ww:else>
    <%
        KeyboardShortcutManager keyboardShortcutManager = ComponentManager.getComponentInstanceOfType(KeyboardShortcutManager.class);
        keyboardShortcutManager.requireShortcutsForContext(KeyboardShortcutManager.Context.issuenavigation);
    %>
<link rel="index" href="<ww:url value="/issuePath" atltoken="false" />" />
<title><ww:text name="'cloneissue.title'" /></title>

<style>
.jiraform td table td{
	padding: 0px;
}

.jiraform td table td table td{
	padding: 5px;
}
</style>





<% 

/* out.println("<script language=\"javascript\">");
out.println("function setCheckboxes"+CHECK_SUBTASK+"(){");
out.println("var value = document.auiform.all"+CHECK_SUBTASK+".checked;");
	
for(Iterator it = subtasks.iterator(); it.hasNext();){
	Issue iss = (Issue) it.next();
	out.println("document.auiform."+ CHECK_SUBTASK +iss.getId()+".checked = value;");
}
out.println("}");

for(Iterator linkIterator = links.getLinkTypes().iterator(); linkIterator.hasNext();){
	// For each link type
	IssueLinkType lt = (IssueLinkType) linkIterator.next();

	// Inward links
	List inwardIssues = links.getInwardIssues(lt.getName());

	if(inwardIssues != null){ 
	// Creating a js function to check all of this type's inward links
	out.println("function setCheckboxes"+CHECK_LINK_IN+lt.getId()+"(){");
	out.println("var value = document.auiform.all"+CHECK_LINK_IN+lt.getId()+".checked;");

	for(Iterator issueIterator = inwardIssues.iterator(); issueIterator.hasNext();){
		Issue issue = (Issue) issueIterator.next();
		out.println("document.auiform."+ CHECK_LINK_IN +"_"+lt.getId()+"_"+issue.getId()+".checked = value;");
	}
	out.println("}");	
	}
	// Outward links
	List outwardIssues = links.getOutwardIssues(lt.getName());
	if(outwardIssues != null){ 
	// Creating a js function to check all of this type's outward links 
	out.println("function setCheckboxes"+CHECK_LINK_OUT+lt.getId()+"(){");
	out.println("var value = document.auiform.all"+CHECK_LINK_OUT+lt.getId()+".checked;");

	for(Iterator issueIterator = outwardIssues.iterator(); issueIterator.hasNext();){
		Issue issue = (Issue) issueIterator.next();
		out.println("document.auiform."+ CHECK_LINK_OUT +"_"+lt.getId()+"_"+issue.getId()+".checked = value;");
	}
	out.println("}");	
	}
}

out.println("</script>"); */
%>



</head>
<body class="type-a">

<ww:if test="/issue">
	<page:applyDecorator id="icio-form" name="auiform">
		<page:param name="title"><ww:text name="'cloneissue.title'"/></page:param>
		<page:param name="action">ImprovedCloneIssueAction.jspa</page:param>
		<page:param name="submitButtonName">Create</page:param>
		<page:param name="submitButtonText"><ww:text name="'cloneissue.create'" /></page:param>
        <page:param name="cancelLinkURI"><ww:url value="/issuePath" atltoken="false"/></page:param>
        

<aui:component template="formDescriptionBlock.jsp" theme="'aui'">
                <aui:param name="'messageHtml'">
                    <p><ww:text name="'cloneissue.step1.desc'" /></p>
                </aui:param>
            </aui:component>
            <%-- if there is no 'clone' link type in the system, print a warning --%>
            <ww:if test="displayCloneLinkWarning == true">
                <aui:component template="auimessage.jsp" theme="'aui'">
                    <aui:param name="'messageType'">warning</aui:param>
                    <aui:param name="'messageHtml'">
                        <p>
                            <ww:text name="'cloneissue.linktype.does.not.exist'">
                                <ww:param name="value0" value="cloneLinkTypeName" />
                            </ww:text>
                        </p>
                    </aui:param>
                </aui:component>
            </ww:if>
            <%-- if the user cannot modify the reporter, print a warning --%>
            <ww:if test="canModifyReporter == false">
                <aui:component template="auimessage.jsp" theme="'aui'">
                    <aui:param name="'messageType'">warning</aui:param>
                    <aui:param name="'messageHtml'">
                        <p><ww:text name="'cloneissue.reporter.modify'" /></p>
                    </aui:param>
                </aui:component>
            </ww:if>

            <aui:component name="'id'" template="hidden.jsp" theme="'aui'"  />


		
		<div>
			<ww:property value="/fieldScreenRenderLayoutItem('summary')/createHtml(/, /, /issueObject)" escape="'false'" />
		</div>
		
		<%-- Subtasks table --%>
		<div style="margin-top: 40px;" >
			
			<ww:if test="/displayCopySubTasks == true">
				
				<tr>
					<strong>
						<td class="fieldLabelArea"><ww:text
							name="'cloneissue.clone.subtasks.label'" />
						</td>
					</strong>
					<td class="fieldValueArea">
					<%= new IssueTableWebComponent().getHtml(itlb, subtasks, null) %>
					</td>
				</tr>	
				<tr>						
					<td class="fieldLabelArea">
						<strong><%= linkLabel %></strong>
					</td>
					<td class="fieldValueArea" bgcolor="#ffffff">
						<input name="cloneSubTasksLinks" value="true" type="checkbox" style="margin-left: 5px;" >
						<br><font size="1"><%= linkDesc %></font>
					</td>
				</tr>
	        </ww:if>
		</div>
		


		<%-- Links --%>
		<div style="margin-top: 40px;" >
			<strong>
				<ww:if test="/displayCopyLink == true">
					<ww:text name="'cloneissue.clone.issuelinks.description'"/>
				</ww:if>
			</strong>
			<jsp:include page="/secure/views/plugins/clone_view_linking.jsp"  />
		</div>
		<%-- <webwork:if test="/displayCopyLink == true">
			<tr>
				<td class="fieldLabelArea"><webwork:text
					name="'cloneissue.clone.issuelinks.label'" /> :</td>
				<td class="fieldValueArea" bgcolor="ffffff" valign="top" colspan="5"
					class="noPadding">
				<table cellspacing="0" border="0" width="100%">
					<webwork:iterator value="linkCollection/linkTypes">
						<webwork:if
							test="/linkCollection/outwardIssues(./name)/empty == false || /linkCollection/inwardIssues(./name)/empty == false">
							<tr>
								<td>
								<table bgcolor="#bbbbbb" class="linktable" border="0"
									cellpadding="3" cellspacing="1" width="100%">
									<tr>
										<td colspan="2" bgcolor="f0f0f0"><b><webwork:property
											value="./name" /></b><br>
										</td>
									</tr>
									<tr>
										<jsp:include page="/secure/views/plugins/outwardLinkEdit.jsp" />
										<jsp:include page="/secure/views/plugins/inwardLinkEdit.jsp" />
										
									</tr>
								</table>
								</td>
							</tr>
						</webwork:if>
					</webwork:iterator>
				</table>

				</td>
			</tr>
			<br />
		</webwork:if> --%>


		<%-- The id of the issue to clone --%>
		<aui:component name="'id'" template="hidden.jsp" theme="'aui'"  />
		
	</page:applyDecorator>
</ww:if>
<ww:else>
	<%-- Issue has been deleted. Show same error as other operations (IssueSummary.jsp) --%>
	<table cellspacing="0" cellpadding="10" border="0" width="100%"
		bgcolor="#ffffff">
		<tr>
			<td valign="top"><page:applyDecorator name="jiraform">
				<page:param name="title">
					<ww:text name="'generic.notloggedin.title'" />
				</page:param>
				<tr>
					<td colspan="2"><ww:text
						name="admin.errors.issues.current.issue.null" /></td>
				</tr>
			</page:applyDecorator></td>
		</tr>
	</table>
</ww:else>

<script language="javascript" >
	String.prototype.startsWith = function(str) 
	{return (this.match("^"+str)==str)}

	 function setCheckboxesSubTaskCheck() {
	    var icioForm = AJS.$('#icio-form');
        var globalChecboxChecked = icioForm.find("input[name='allSubTaskCheck']").is(':checked');

        icioForm.find("input[type='checkbox']").each(function() {
            if (AJS.$(this).attr('name').startsWith('SubTaskCheck')) {
                if (globalChecboxChecked) {
                    AJS.$(this).attr('checked', 'checked');
                } else {
                    AJS.$(this).removeAttr('checked');
                }
            }
        });
	}
</script>

</body>
</html>