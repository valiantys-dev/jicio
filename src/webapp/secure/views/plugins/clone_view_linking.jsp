<%@ taglib uri="webwork" prefix="ww"%>

<%
final String CHECK_LINK_IN="LinkCheckIn";
final String CHECK_LINK_OUT="LinkCheckOut";
%>

<ww:if
	test="(applicationProperties/option('jira.option.issuelinking') == true && linkCollection/displayLinkPanel == true)">
	<div class="module toggle-wrap" id="linkingmodule">
		<div class="mod-header">
			<ww:if test="/linkable == true">
				<ul class="ops">
					<li><a id="add-links-link"
						href="<ww:url page="/secure/LinkExistingIssue!default.jspa"><ww:param name="'id'" value="../long('id')" /></ww:url>"
						class="issueaction-link-issue icon icon-add16"
						title="<ww:text name="'viewissue.links.addlink'"/>"><span><ww:text
									name="'viewissue.links.addlink'" />
						</span>
					</a>
					</li>
				</ul>
			</ww:if>
			<h3 class="toggle-title">
				<ww:text name="'common.concepts.issuelinks'" />
			</h3>
		</div>
		<div class="mod-content">
			<ww:iterator value="linkCollection/linkTypes"
				status="'itLinkTypeStatus'">
				<ww:if
					test="/linkCollection/outwardIssues(./name)/empty == false || /linkCollection/inwardIssues(./name)/empty == false">
					<ww:property value="/linkCollection/outwardIssues(./name)">
						<ww:if test=". != null && ./empty == false">
							<table class="aui issue-links links-outward aui-table-rowhover">
								<thead>
									<tr>
										<td colspan="5"><ww:text
												name="'common.concepts.linkDescription'">
												<ww:param name="'value0'">
													<strong><ww:property value="../outward" />
													</strong>
												</ww:param>
											</ww:text>:</td>
									</tr>
								</thead>
								<tbody>
									<ww:iterator>
										<tr>
											<td style="width: 5px; height: 5px;" >
												<input type='checkbox' name='<%=CHECK_LINK_OUT%>_<ww:property value="../id" />_<ww:property value="string('id')"/>' />
											</td>
											<td class="issuekey"><a
												href="<ww:url value="'/browse/' + string('key')" atltoken="false" />"
												title="<ww:property value="string('summary')" />"
												<ww:if test="string('resolution')" >class="resolution"</ww:if>><ww:property
														value="string('key')" />
											</a></td>
											<td class="summary"><ww:property
													value="string('summary')" /></td>
											<td class="priority"><ww:if
													test=". != null && @fieldVisibility/fieldHidden('priority', .) == false">
													<%@ include file="/includes/icons/priority.jsp"%>
												</ww:if></td>
											<td class="status"><%@ include
													file="/includes/icons/status.jsp"%>
											</td>
										</tr>
									</ww:iterator>
								</tbody>
							</table>
						</ww:if>
					</ww:property>

					<ww:property value="/linkCollection/inwardIssues(./name)">
						<ww:if test=". != null && ./empty == false">
							<table class="aui issue-links links-inward aui-table-rowhover">
								<thead>
									<tr>
										<td colspan="5"><ww:text
												name="'common.concepts.linkDescription'">
												<ww:param name="'value0'">
													<strong><ww:property value="../inward" />
													</strong>
												</ww:param>
											</ww:text>:</td>
									</tr>
								</thead>
								<tbody>
									<ww:iterator>
										<tr>
											<td style="width: 5px; height: 5px;" >
												<input type='checkbox' name='<%=CHECK_LINK_IN%>_<ww:property value="../id" />_<ww:property value="string('id')"/>'/>
											</td>
											<td class="issuekey">
												<a href="<ww:url value="'/browse/' + string('key')" atltoken="false" />"
													title="<ww:property value="string('summary')" />"
													<ww:if test="string('resolution')" >class="resolution"</ww:if>
												>
													<ww:property value="string('key')" />
												</a>
											</td>
											<td class="summary"><ww:property
													value="string('summary')" /></td>
											<td class="priority"><ww:if
													test=". != null && @fieldVisibility/fieldHidden('priority', .) == false">
													<%@ include file="/includes/icons/priority.jsp"%>
												</ww:if></td>
											<td class="status"><%@ include
													file="/includes/icons/status.jsp"%>
											</td>
										</tr>
									</ww:iterator>
								</tbody>
							</table>
						</ww:if>
					</ww:property>
				</ww:if>
			</ww:iterator>
		</div>
	</div>
</ww:if>
